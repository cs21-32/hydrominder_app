export const state = () => ({
  spinner: false,
  lastShownFillPercentage: 0,
  consumptionStatus: {
    currentAmount: 0,
    expectedCurrentAmount: undefined,
    goal: undefined,
    shouldConsume: true,
    shouldConsumeAmount: 0
  },
  daysReached: 0,
  bestWeekDay: 'monday',
  bestMonthDay: '1st'
})

export const mutations = {
  setFillPercentage(state, fillPercentage) {
    state.lastShownFillPercentage = fillPercentage
  },

  setConsumptionStatus(state, status) {
    Object.keys(state.consumptionStatus).forEach((k) => {
      // eslint-disable-next-line security/detect-object-injection
      if (state.consumptionStatus[k] !== status[k]) {
        // eslint-disable-next-line security/detect-object-injection
        state.consumptionStatus[k] = status[k]
      }
    })
  },

  setSpinner(state, value) {
    state.spinner = value
  },

  setDaysReached(state, daysReached) {
    state.daysReached = daysReached
  },

  setBestWeekDay(state, bestWeekDay) {
    state.bestWeekDay = bestWeekDay
  },

  setBestMonthDay(state, bestMonthDay) {
    state.bestMonthDay = bestMonthDay
  }
}

export const getters = {
  lastShownFillPercentage(state) {
    return state.lastShownFillPercentage
  },
  goal(state) {
    return state.consumptionStatus.goal
  },
  consumption(state) {
    return state.consumptionStatus.currentAmount
  },
  expectedConsumption(state) {
    return state.consumptionStatus.expectedCurrentAmount
  },
  dropletFillPercentage(state) {
    return Math.min(100, Math.floor(state.consumptionStatus.currentAmount / state.consumptionStatus.expectedCurrentAmount * 100))
  },
  trackerFillPercentage(state) {
    return Math.min(100, Math.floor(state.consumptionStatus.currentAmount / state.consumptionStatus.goal * 100))
  },
  spinner(state) {
    return state.spinner
  },
  daysReached(state) {
    return state.daysReached
  },
  bestWeekDay(state) {
    return state.bestWeekDay
  },
  bestMonthDay(state) {
    return state.bestMonthDay
  }
}

export const actions = {
  async fetchAllData(context, progress = true) {
    const consumptionStatus = await this.$axios.get('/api/consumption/status', { progress })
    context.commit('setConsumptionStatus', consumptionStatus.data)
  }
}
