FROM node:15 AS build-env

WORKDIR /usr/src/app

# Environment variables for production

COPY package*.json ./
RUN yarn install
COPY . .
RUN yarn run build
RUN yarn run generate

FROM nginx:alpine
COPY --from=build-env /usr/src/app/dist /usr/share/nginx/html
ENV NODE_ENV=production
