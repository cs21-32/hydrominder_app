# HydroMinderApp

<div align="center"> 
  <p align="center">
    <img src="https://i.imgur.com/uBzEyGT.png" width="256" alt="HydroMinder Logo" />
  </p>

  <h1 align="center">HydroMinder Web App</h1>
  <p align="center">A modern hydration tracker for anyone.</p>
</div>

## Description

This is the Web Application.

## Development Requirements

- NodeJS 16.X
- Yarn Package Manager

## Installation

```bash
$ yarn
```

## Running the app

Make sure to make a copy of the `.env.example` file as `.env`. The default values will work for development.
For development, it is recommended to also run the [HydroMinder API](https://gitlab.utwente.nl/cs21-32/hydrominder_api).


```bash
# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Test

```bash
# unit tests
$ yarn run test
```

## Publish New Container

```bash
# Login to the registry
docker login registry.gitlab.utwente.nl

# Build with tag 'latest' by default
docker build . --platform linux/arm64 -t registry.gitlab.utwente.nl/cs21-32/hydrominder_app

# Push release
docker push registry.gitlab.utwente.nl/cs21-32/hydrominder_app
```

## License

HydroMinder Web App is [MIT licensed](LICENSE).
