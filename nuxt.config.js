export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'HydroMinderApp',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  env: {
    mode: process.env.NODE_ENV || 'production',
    port: process.env.PORT || 3000
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/windi.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/vue-apexcharts.js',
    '@/plugins/axios.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build'
    // 'nuxt-windicss'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/axios',
    '@nuxtjs/auth-next'
  ],

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  router: {
    middleware: ['auth']
  },

  axios: {
    proxy: process.env.NODE_ENV === 'development'
  },

  proxy: {
    '/api/': {
      target: 'http://localhost:3001/',
      pathRewrite: { '^/api/': '' }
    }
  },

  auth: {
    strategies: {
      cookie: {
        // cookie: {
        //   name: 'connect.sid'
        // },
        endpoints: {
          login: {
            url: 'api/auth/login',
            method: 'post'
          },
          logout: {
            url: 'api/auth/logout',
            method: 'post'
          },
          user: {
            url: 'api/auth/me',
            method: 'get'
          }
        },
        user: {
          property: false
        }
      }
    },
    redirect: {
      logout: '/login'
    }
  }
}
