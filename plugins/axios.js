export default function({ $axios }) {
  // based on https://github.com/nuxt-community/axios-module/issues/341#issuecomment-650162637
  if ((process.client || process.static) && process.env.NODE_ENV !== 'development') {
    $axios.setBaseURL('//' + window.location.hostname + '/')
  }
}
